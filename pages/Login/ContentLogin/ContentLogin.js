import { Text } from "@nextui-org/react";
import { Box } from "../../Box";
import { Input, Spacer, Button, Grid } from "@nextui-org/react";

import React from "react";

export default function ContentLogin() {
  return (
    <Box
      css={{
        textAlign: "center",
        px: "$12",
        mt: "$10",
        "@xsMax": { px: "$10" },
      }}
    >
      <Text
        h2
        size={40}
        css={{
          textGradient: "45deg, $purple600 -20%, $pink600 100%",
        }}
        weight="bold"
      >
        Sign in to Celebration
      </Text>
      <Grid>
        <Input underlined labelLeft="username" placeholder="Input Username" />
      </Grid>
      <Grid>
        <Input
          underlined
          labelLeft="password"
          placeholder="Input Password"
          type="password"
        />
      </Grid>
      <Spacer y={0.6} />
      <Button Link="/Profile" css={{ position: "relative", left: "43%" }}>
        Login
      </Button>
      ;<Text size="$sm"></Text>
    </Box>
  );
}
