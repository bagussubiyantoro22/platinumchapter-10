import Header from "../Header";
import SignupPage from "./ContentSignup/ContentSignup.js";

export default function ContentSignup() {
  return (
    <>
      <Header />
      <SignupPage />
      <title>Binar | Kelompok-2</title>
    </>
  );
}
